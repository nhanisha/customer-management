package com.hanisha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManageCustomersApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManageCustomersApplication.class, args);
	}
}
