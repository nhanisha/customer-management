package com.hanisha.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hanisha.models.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

	Customer findOneById(Long companyId);
	
	List<Customer> findAllByCompanyId(Long id);

}
