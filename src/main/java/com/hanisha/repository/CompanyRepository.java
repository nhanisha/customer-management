package com.hanisha.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hanisha.models.Company;

public interface CompanyRepository extends JpaRepository<Company,Long> {

	public Company findOneById(Long companyId);
	
	public Company findOneByUniqueCode(String uniqueCode);

}
