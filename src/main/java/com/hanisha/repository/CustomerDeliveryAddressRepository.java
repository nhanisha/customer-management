package com.hanisha.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hanisha.models.Company;
import com.hanisha.models.CustomerDeliveryAddress;

public interface CustomerDeliveryAddressRepository extends JpaRepository<CustomerDeliveryAddress, Long> {

	CustomerDeliveryAddress findOneById(Long customerDeliveryAddressID);
	
	CustomerDeliveryAddress findOneByIdAndCustomerId(Long id,Long customerId);

	List<CustomerDeliveryAddress> findAllByCustomerId(Long id);
	
}
