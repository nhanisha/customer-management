package com.hanisha.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanisha.models.Company;
import com.hanisha.repository.CompanyRepository;

@Service
public class CompanyService {
	
	@Autowired
	CompanyRepository companyRepository;

/*to save an company*/
public Company save(Company company) {
	return companyRepository.save(company);
}

/*search all company*/
public List <Company> findAll(){
	return companyRepository.findAll();
}

/*get an company by id*/
public Company findOne(Long companyId) {
	return companyRepository.findOneById(companyId);
}
/*delete an company */
public void delete(Company company) {
	companyRepository.delete(company);
}


}
