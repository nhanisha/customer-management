package com.hanisha.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanisha.models.CustomerDeliveryAddress;
import com.hanisha.repository.CustomerDeliveryAddressRepository;
@Service
public class CustomerDeliveryAddressService {

	@Autowired
	CustomerDeliveryAddressRepository customerDeliveryAddressRepository;

/*to save a new customer delivery address.*/
public CustomerDeliveryAddress save(CustomerDeliveryAddress customerDeliveryAddress) {
	return customerDeliveryAddressRepository.save(customerDeliveryAddress);
}

/*search all customer delivery address.*/
public List <CustomerDeliveryAddress> findAll(){
	return customerDeliveryAddressRepository.findAll();
}

/*get an customer delivery address by id*/
public CustomerDeliveryAddress findOne(Long customerDeliveryAddressID) {
	return customerDeliveryAddressRepository.findOneById(customerDeliveryAddressID);
}
/*delete an company */
public void delete(CustomerDeliveryAddress customerDeliveryAddress) {
	customerDeliveryAddressRepository.delete(customerDeliveryAddress);
}

}
