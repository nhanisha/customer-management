package com.hanisha.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanisha.dto.CustomerDTO;
import com.hanisha.models.Company;
import com.hanisha.models.Customer;
import com.hanisha.repository.CompanyRepository;
import com.hanisha.repository.CustomerRepository;

@Service
public class CustomerService {

	@Autowired
	CustomerRepository customerRepository;
	

/*to save an company*/
public Customer save(Customer customer) {
	return customerRepository.save(customer);
	
}

/*search all company*/
public List <Customer> findAll(){
	return customerRepository.findAll();
}

/*get an company by id*/
public Customer findOne(Long companyId) {
	return customerRepository.findOneById(companyId);
}
///*delete an company */
//public void delete(Company company) {
//	customerRepository.delete(company);
//}
}
