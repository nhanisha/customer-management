package com.hanisha.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanisha.dto.CustomerDTO;
import com.hanisha.dto.ErrorResponseDTO;
import com.hanisha.models.Company;
import com.hanisha.models.Customer;
import com.hanisha.repository.CompanyRepository;
import com.hanisha.repository.CustomerRepository;
import com.hanisha.services.CompanyService;
import com.hanisha.services.CustomerService;

@RestController
@RequestMapping(path="customer")
public class CustomerController {
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	CompanyRepository companyRepository;
	
	@Autowired
	CustomerRepository customerRepository;
	
    /*to save an customer*/
	@PostMapping()
	public ResponseEntity<?> createCustomer(@Valid @RequestBody CustomerDTO customerDetails) {
		
		Company company= companyRepository.findOneByUniqueCode(customerDetails.getCompanyUniqueCode());
		if(company==null) {
			ErrorResponseDTO errorResponseDTO= new ErrorResponseDTO("Company is not found eith given companyUniqueCode "+customerDetails.getCompanyUniqueCode());
			return ResponseEntity.badRequest().body(errorResponseDTO);
		}
		
		Customer customer= new Customer();
		customer.setName(customerDetails.getName());
		customer.setAge(customerDetails.getAge());
		customer.setEmail(customerDetails.getEmail());
		customer.setPassword(customerDetails.getPassword());
		customer.setPhoneNumber(customerDetails.getPhoneNumber());
		customer.setCompany(company);
		customer.setAddress(customerDetails.getAddress());
		
		Customer savedCustomer=customerService.save(customer);
		
		return ResponseEntity.ok().body(savedCustomer);
				
	}
	
	/*get all customers*/
	@GetMapping("/{comapnyId}")
	public List<Customer> getAllCustomers( @PathVariable("comapnyId") Long companyId){
		return customerRepository.findAllByCompanyId(companyId);
	}
	
	/*update n company by companyId*/
	@PutMapping("/{id}")
	public ResponseEntity<?> updateCustomers(@PathVariable(value="id")Long customerId,@Valid @RequestBody CustomerDTO customerDetails){
		Customer customer=customerRepository.findOneById(customerId);
		if(customer==null) {
			ErrorResponseDTO errorResponseDTO= new ErrorResponseDTO("Customer is not found eith given customerId "+customerId);
			return ResponseEntity.badRequest().body(errorResponseDTO);
			
		}
		
		customer.setName(customerDetails.getName());
		customer.setAge(customerDetails.getAge());
		customer.setEmail(customerDetails.getEmail());
		customerDetails.setPhoneNumber(customerDetails.getPhoneNumber());
		customer.setAddress(customerDetails.getAddress());
		
		Customer updatedCustomer=customerService.save(customer);
		return ResponseEntity.ok().body(updatedCustomer);
	}
	
	/*delete an employee*/
	@DeleteMapping("/{id}")
	public ResponseEntity<?> disableCustomer(@PathVariable(value="id") Long customerId){
		Customer customer=customerService.findOne(customerId);
		if(customer==null) {
			ErrorResponseDTO errorResponseDTO= new ErrorResponseDTO("Customer is not found eith given companyId "+customerId);
			return ResponseEntity.badRequest().body(errorResponseDTO);
			
		}
		customer.setStatus("INACTIVE");
		Customer disabledCustomer=customerService.save(customer);
		return ResponseEntity.ok().body(disabledCustomer);
	}
}
