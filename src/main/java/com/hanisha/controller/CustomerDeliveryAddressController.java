package com.hanisha.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanisha.dto.CustomerDTO;
import com.hanisha.dto.CustomerDeliveryAddressDTO;
import com.hanisha.dto.ErrorResponseDTO;
import com.hanisha.dto.UpdateCustomerDeliveryAddressDTO;
import com.hanisha.models.Company;
import com.hanisha.models.Customer;
import com.hanisha.models.CustomerDeliveryAddress;
import com.hanisha.repository.CompanyRepository;
import com.hanisha.repository.CustomerDeliveryAddressRepository;
import com.hanisha.repository.CustomerRepository;
import com.hanisha.services.CustomerDeliveryAddressService;
import com.hanisha.services.CustomerService;

@RestController
@RequestMapping("/customer-devlivery")
public class CustomerDeliveryAddressController {


	@Autowired
	CustomerService customerService;
	
	@Autowired
	CustomerDeliveryAddressRepository customerDeliveryAddressRepository;  
	
	@Autowired
	CustomerDeliveryAddressService customerDeliveryAddressService; 
	
	@Autowired
	CustomerRepository customerRepository; 
	
    /*
     * to save an customer delivery adddress
     */
	@PostMapping()
	public ResponseEntity<?> createCustomerDeliveryAddress(@Valid @RequestBody CustomerDeliveryAddressDTO customerDeliveryAddressDetails) {
		
		Customer customer= customerRepository.findOneById(customerDeliveryAddressDetails.getCustomerId());
		if(customer==null) {
			ErrorResponseDTO errorResponseDTO= new ErrorResponseDTO("Customer is not found eith given customreId "+customerDeliveryAddressDetails.getCustomerId());
			return ResponseEntity.badRequest().body(errorResponseDTO);
		}
		
		CustomerDeliveryAddress customerDeliveryAddress= new CustomerDeliveryAddress();
		customerDeliveryAddress.setCustomer(customer);
		customerDeliveryAddress.setDeliveryAddress(customerDeliveryAddressDetails.getAddress());
		
		return ResponseEntity.ok().body(customerDeliveryAddressService.save(customerDeliveryAddress));
	}
	
	/*
	 * get all customers delivery address
	 */
	@GetMapping("/{customerId}")
	public ResponseEntity<?> getCustomerDeliveryAddress( @PathVariable("customerId") Long customerId){
		Customer customer= customerRepository.findOneById(customerId);
		if(customer==null) {
			ErrorResponseDTO errorResponseDTO= new ErrorResponseDTO("Customer is not found eith given customreId "+customerId);
			return ResponseEntity.badRequest().body(errorResponseDTO);
		}
		List<CustomerDeliveryAddress> customerDeliveryAddresses=customerDeliveryAddressRepository.findAllByCustomerId(customer.getId());
		return ResponseEntity.ok().body(customerDeliveryAddresses);
	}
	
	/*
	 * update customers delivery address by customerId and addressId
	 * */
	@PutMapping("/{customerId}/{addressId}")
	public ResponseEntity<?> updateCustomers(@PathVariable("customerId")Long customerId,@PathVariable("addressId")Long addressId,@Valid @RequestBody UpdateCustomerDeliveryAddressDTO customerDeliveryAddressDTO){
		Customer customer=customerRepository.findOneById(customerId);
		if(customer==null) {
			ErrorResponseDTO errorResponseDTO= new ErrorResponseDTO("Customer is not found eith given customreId "+customerId);
			return ResponseEntity.badRequest().body(errorResponseDTO);	
		}
		
		CustomerDeliveryAddress customerDeliveryAddress= customerDeliveryAddressService.findOne(addressId);
		if(customerDeliveryAddress==null) {
			ErrorResponseDTO errorResponseDTO= new ErrorResponseDTO("Customer devliver address is not found eith given customreDeliverAddressId "+addressId);
			return ResponseEntity.badRequest().body(errorResponseDTO);
		}
		
		customerDeliveryAddress.setDeliveryAddress(customerDeliveryAddressDTO.getAddress());
		CustomerDeliveryAddress updatedCustomerDeliveryAddress= customerDeliveryAddressService.save(customerDeliveryAddress);
		return ResponseEntity.ok().body(updatedCustomerDeliveryAddress);
	}
	
	
	/*
	 * disables the  an customers delivery address by using customerid and addressid
	 * */
	@DeleteMapping("/{customerId}/{addressId}")
	public ResponseEntity<?> disableCustomer(@PathVariable("customerId") Long customerId,@PathVariable("addressId") Long id){
		Customer customer=customerRepository.findOneById(customerId);
		if(customer==null) {
			ErrorResponseDTO errorResponseDTO= new ErrorResponseDTO("Customer is not found eith given customreId "+customerId);
			return ResponseEntity.badRequest().body(errorResponseDTO);
			
		}
		
		CustomerDeliveryAddress customerDeliveryAddress= customerDeliveryAddressRepository.findOneByIdAndCustomerId(id, customerId);
		if(customerDeliveryAddress==null) {
			ErrorResponseDTO errorResponseDTO= new ErrorResponseDTO("Customer devliver address is not found eith given customreDeliverAddressId "+id);
			return ResponseEntity.badRequest().body(errorResponseDTO);
		}
		customerDeliveryAddress.setStatus("INACTIVE");
		CustomerDeliveryAddress disabledCustomerDeliveryAddress=customerDeliveryAddressService.save(customerDeliveryAddress);
		return ResponseEntity.ok().body(disabledCustomerDeliveryAddress);
	}
}
