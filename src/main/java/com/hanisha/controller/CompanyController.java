package com.hanisha.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanisha.dto.ErrorResponseDTO;
import com.hanisha.models.Company;
import com.hanisha.repository.CompanyRepository;
import com.hanisha.services.CompanyService;

@RestController
@RequestMapping(path="/company")
public class CompanyController {

	@Autowired
	CompanyService companyService;
	
	@Autowired
	CompanyRepository companyRepository;
	
    /*to save an company*/
	@PostMapping()
	public Company createCompany(@Valid @RequestBody Company company) {
//		company.setStatus("ACTIVE");
		return companyService.save(company);
	}
	
	/*get all company*/
	@GetMapping
	public List<Company> getAllCompany(){
		return companyService.findAll();
	}
	
	/*get company by companyId*/
	@GetMapping("/{id}")
	public ResponseEntity<?> getCompanyId(@PathVariable(value="id") Long companyId){
		Company company=companyService.findOne(companyId);
		if(company==null) {
			ErrorResponseDTO errorResponseDTO= new ErrorResponseDTO("Company is not found eith given companyId "+companyId);
			return ResponseEntity.badRequest().body(errorResponseDTO);
			
		}
		return ResponseEntity.ok().body(company);
		
	}
	/*update n company by companyId*/
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updateCompany(@PathVariable(value="id")Long companyId,@Valid@RequestBody Company companyDetails){
		Company company=companyService.findOne(companyId);
		if(company==null) {
			
			ErrorResponseDTO errorResponseDTO= new ErrorResponseDTO("Company is not found eith given companyId "+companyId);
			return ResponseEntity.badRequest().body(errorResponseDTO);
			
		}
		
		company.setName(companyDetails.getName());
		company.setWebsite(companyDetails.getWebsite());
		company.setAddress(companyDetails.getAddress());
		company.setStatus(companyDetails.getStatus());
		company.setUniqueCode(companyDetails.getUniqueCode());
		
		Company updateCompany=companyService.save(company);
		return ResponseEntity.ok().body(updateCompany);
	}
	
	/*delete an employee*/
	@DeleteMapping("/{id}")
	public ResponseEntity<Company> deleteCompany(@PathVariable(value="id") Long companyId){
		Company company=companyService.findOne(companyId);
		if(company==null) {
			return ResponseEntity.notFound().build();
			
		}
		company.setStatus("INACTIVE");
		Company disabledCompany= companyRepository.save(company);
		return ResponseEntity.ok().body(disabledCompany);
	}
}
