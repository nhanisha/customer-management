package com.hanisha.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class CustomerDeliveryAddressDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2253926327986504676L;

	@NotNull
	public String address;
	
	@NotNull
	public Long customerId;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
}
