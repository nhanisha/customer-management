package com.hanisha.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CustomerDTO implements Serializable {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 4561877593490462631L;
	
	@NotBlank
	public String name;
	
	public int age;
	
	@NotBlank
	@Email
	public String email;
	
	@NotBlank
	public String phoneNumber;
	
	@NotNull
	public String address;
	
	@NotNull
	public String companyUniqueCode;
	
	public String status;
	
	@NotBlank
	private String password;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCompanyUniqueCode() {
		return companyUniqueCode;
	}

	public void setCompanyUniqueCode(String companyUniqueCode) {
		this.companyUniqueCode = companyUniqueCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
