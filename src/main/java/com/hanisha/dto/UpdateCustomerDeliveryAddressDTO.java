package com.hanisha.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class UpdateCustomerDeliveryAddressDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8661709410862363417L;
	
	@NotNull
	public String address;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	
}
