package com.hanisha.dto;

import java.io.Serializable;

public class ErrorResponseDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4578893742398946928L;
	
	public String errorReponse;

	public ErrorResponseDTO(String errorReponse) {
		super();
		this.errorReponse = errorReponse;
	}
}
