This project mainly aims at managing the customers for a company.
To run the project Follow the below steps.

TO RUN IN ECLIPSE:-
1. Close the reposity and go into the directory using cd command.
2. Import the project into the eclipse using eclipse import.
3. Modify the `spring.datasource` properties as per your environment under `application.properties` file. (Db name, username, password etc..)
4. Right click on the project and choose run as --> then java application --> ManageCustomersApplications.java.

TO RUN A JAR FULE:-

Follow above first 3 steps.  
4. Run  `mvn build -DskipTests=true`. This command will build the project and generates a `.jar` file in `target` directory of the project.  
5. run `cd target`  
6. run `java -jar generated jar file`  

Full documentation of the project is available under [WIKI section](https://gitlab.com/nhanisha/customer-management/wikis/Introduction-to-Customer-Management).
